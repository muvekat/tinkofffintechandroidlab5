package com.example.movie.tinkofffintechandroidlab5.utils

import java.text.DateFormat
import java.util.*

fun getFormattedDate(time: Long): String{
    return DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.getDefault())
        .format(Date(time * 1000))
}