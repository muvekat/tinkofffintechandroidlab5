package com.example.movie.tinkofffintechandroidlab5.data.source

import com.example.movie.tinkofffintechandroidlab5.data.Currency

interface CurrencyDataSource{

    interface LoadCurrenciesCallback {

        fun onCurrenciesLoaded(currencies: List<Currency>)

        fun onDataNotAvailable(throwable: Throwable)
    }

    fun getCurrencies(callback: LoadCurrenciesCallback)
}