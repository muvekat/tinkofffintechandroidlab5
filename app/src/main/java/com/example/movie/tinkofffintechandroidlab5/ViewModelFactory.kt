package com.example.movie.tinkofffintechandroidlab5

import android.annotation.SuppressLint
import android.app.Application
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.example.movie.tinkofffintechandroidlab5.currencies.CurrenciesViewModel
import com.example.movie.tinkofffintechandroidlab5.data.source.CurrencyRepository
import com.example.movie.tinkofffintechandroidlab5.details.CurrencyDetailsViewModel

class ViewModelFactory private constructor(
    private val currencyRepository: CurrencyRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>) =
        with(modelClass) {
            when {
                isAssignableFrom(CurrenciesViewModel::class.java) ->
                    CurrenciesViewModel(currencyRepository)
                isAssignableFrom(CurrencyDetailsViewModel::class.java) ->
                    CurrencyDetailsViewModel(currencyRepository)
                else ->
                    throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
            }
        } as T

    companion object {

        @SuppressLint("StaticFieldLeak")
        @Volatile private var INSTANCE: ViewModelFactory? = null

        fun getInstance() =
            INSTANCE ?: synchronized(ViewModelFactory::class.java) {
                INSTANCE ?: ViewModelFactory(CurrencyRepository.getInstance())
                    .also { INSTANCE = it }
            }
    }
}