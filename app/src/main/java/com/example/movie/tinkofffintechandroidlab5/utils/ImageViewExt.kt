package com.example.movie.tinkofffintechandroidlab5.utils

import android.app.Activity
import android.view.View
import android.widget.ImageView
import com.bumptech.glide.Glide

fun ImageView.setImageFromUrl(tiedView: View, url: String){
    Glide.with(tiedView)
        .load(url)
        .into(this)
}

fun ImageView.setImageFromUrl(tiedActivity: Activity, url: String){
    Glide.with(tiedActivity)
        .load(url)
        .into(this)
}