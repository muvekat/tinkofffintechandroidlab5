package com.example.movie.tinkofffintechandroidlab5.currencies

import com.example.movie.tinkofffintechandroidlab5.data.Currency

interface CurrencyItemUserActionsListener {
    fun onCurrencyClicked(currency: Currency)
}