package com.example.movie.tinkofffintechandroidlab5.currencies

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.example.movie.tinkofffintechandroidlab5.SingleLiveEvent
import com.example.movie.tinkofffintechandroidlab5.data.Currency
import com.example.movie.tinkofffintechandroidlab5.data.source.CurrencyDataSource
import com.example.movie.tinkofffintechandroidlab5.data.source.CurrencyRepository

class CurrenciesViewModel(private val currenyRepository: CurrencyRepository): ViewModel(){

    internal val openCurrencyEvent = SingleLiveEvent<Currency>()

    // These observable fields will update Views
    val items: MutableLiveData<List<Currency>> = MutableLiveData()
    val contentVisible = MutableLiveData<Boolean>().apply { postValue(true) }
    val dataLoading = MutableLiveData<Boolean>().apply { postValue(false) }
    val errorHappened = MutableLiveData<Boolean>().apply { postValue(false) }

    //Requesting currencies only once per ViewModel creation so that on screen rotation we are observing cached items.
    init {
        this.requestCurrencies()
    }

    fun requestCurrencies(){
        dataLoading.postValue(true)
        currenyRepository.getCurrencies(object: CurrencyDataSource.LoadCurrenciesCallback{
            override fun onCurrenciesLoaded(currencies: List<Currency>) {
                items.postValue(currencies)
                contentVisible.postValue(true)
                dataLoading.postValue(false)
                errorHappened.postValue(false)
            }

            override fun onDataNotAvailable(throwable: Throwable) {
                dataLoading.postValue(false)
                contentVisible.postValue(false)
                errorHappened.postValue(true)
            }
        })
    }

    fun openCurrency(currency: Currency){
        openCurrencyEvent.postValue(currency)
    }


}