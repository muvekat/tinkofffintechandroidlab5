package com.example.movie.tinkofffintechandroidlab5.utils

import android.support.v4.content.ContextCompat
import android.widget.TextView
import com.example.movie.tinkofffintechandroidlab5.R

fun <T> TextView.setTextFromFormat(stringFormat: Int, vararg formatArgs:T){
    text = context.getString(stringFormat, *formatArgs)
}

fun TextView.setTextPrice(price: String?){
    if (price != null) setTextFromFormat(R.string.price_format, price)
}

fun TextView.setTextPercentChange(percentChange: Double){
    setTextFromFormat(R.string.percent_format, percentChange)
}

fun TextView.setTextPercentChangeAndAppropriateColor(percentChange: Double){
    setTextPercentChange(percentChange)
    if (percentChange > 0)
        this.setTextColor(ContextCompat.getColor(context, R.color.green700))
    else
        this.setTextColor(ContextCompat.getColor(context, R.color.red700))
}