package com.example.movie.tinkofffintechandroidlab5.data.source

import com.example.movie.tinkofffintechandroidlab5.data.Currency
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CurrencyRepository: CurrencyDataSource{

    override fun getCurrencies(callback: CurrencyDataSource.LoadCurrenciesCallback) {
        NetworkRequest.apiService
            .getCoinsList()
            .enqueue(object : Callback<List<Currency>> {
                override fun onResponse(call: Call<List<Currency>>, response: Response<List<Currency>>) {
                    if (response.body() != null)
                        callback.onCurrenciesLoaded(response.body()!!)
                }

                override fun onFailure(call: Call<List<Currency>>, t: Throwable) {
                    callback.onDataNotAvailable(t)

                }
            })
    }

    companion object {

        private var INSTANCE: CurrencyRepository? = null

        @JvmStatic fun getInstance() =
            INSTANCE ?: synchronized(CurrencyRepository::class.java) {
                INSTANCE ?: CurrencyRepository()
                    .also { INSTANCE = it }
            }

        @JvmStatic fun destroyInstance() {
            INSTANCE = null
        }
    }
}