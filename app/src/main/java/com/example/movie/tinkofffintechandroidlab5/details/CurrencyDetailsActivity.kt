package com.example.movie.tinkofffintechandroidlab5.details

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.movie.tinkofffintechandroidlab5.R
import com.example.movie.tinkofffintechandroidlab5.currencies.CurrenciesViewModel
import com.example.movie.tinkofffintechandroidlab5.utils.*
import java.text.DateFormat
import java.util.*

class CurrencyDetailsActivity: AppCompatActivity(){

    companion object {
        const val CURRENCY_JSON = "currencyJson"
    }

    private var toolbar: Toolbar? = null
    private var logo: ImageView? = null
    private var title: TextView? = null
    private var price: TextView? = null
    private var change7d: TextView? = null
    private var change24h: TextView? = null
    private var change1h: TextView? = null
    private var marketCap: TextView? = null
    private var lastUpdate: TextView? = null
    private lateinit var viewModel: CurrencyDetailsViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)

        viewModel = obtainViewModel()
        viewModel.start(intent.getStringExtra(CURRENCY_JSON))

        setupViews()

        setupObservers()
    }

    private fun setupViews(){
        logo = findViewById(R.id.coin_logo)
        title = findViewById(R.id.coin_title)
        price = findViewById(R.id.price_value)
        change7d = findViewById(R.id.change_value_7d)
        change24h = findViewById(R.id.change_value_24h)
        change1h = findViewById(R.id.change_value_1h)
        marketCap = findViewById(R.id.market_cap_value)
        lastUpdate = findViewById(R.id.last_update_value)

        toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
    }

    private fun setupObservers(){
        viewModel.apply{

            item.observe(this@CurrencyDetailsActivity, android.arch.lifecycle.Observer {currency ->
                if (currency != null) {
                    supportActionBar?.title = currency.name
                    val logoUrl = getString(R.string.coin_logo_url, currency.symbol!!.toLowerCase())
                    logo?.setImageFromUrl(this@CurrencyDetailsActivity, logoUrl)
                    title?.text = currency.name
                    price?.setTextPrice(currency.priceUsd)
                    change7d?.setTextPercentChangeAndAppropriateColor(currency.percentChange7d)
                    change24h?.setTextPercentChangeAndAppropriateColor(currency.percentChange24h)
                    change1h?.setTextPercentChangeAndAppropriateColor(currency.percentChange1h)
                    marketCap?.setTextPrice(currency.marketCapUsd)
                    lastUpdate?.text = getFormattedDate(currency.lastUpdated)
                }
            })

        }
    }

    private fun obtainViewModel(): CurrencyDetailsViewModel = obtainViewModel(CurrencyDetailsViewModel::class.java)
}