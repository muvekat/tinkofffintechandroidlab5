package com.example.movie.tinkofffintechandroidlab5.currencies

import android.arch.lifecycle.Observer
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.example.movie.tinkofffintechandroidlab5.R
import com.example.movie.tinkofffintechandroidlab5.data.Currency
import com.example.movie.tinkofffintechandroidlab5.details.CurrencyDetailsActivity
import com.example.movie.tinkofffintechandroidlab5.utils.obtainViewModel

class CurrenciesActivity : AppCompatActivity(), CurrencyItemNavigator {

    private var adapter: CurrenciesAdapter? = null
    private var errorView: View? = null
    private var currenciesView: View? = null
    private var loadingView: View? = null
    private lateinit var viewModel: CurrenciesViewModel
    private lateinit var currencyClickedListener: CurrencyItemUserActionsListener

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currencies)
        viewModel = obtainViewModel()
        currencyClickedListener = getCurrencyClickedListener()

        setupViews()
        setupObservers()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == R.id.action_refresh) {
            viewModel.requestCurrencies()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    private fun setupViews(){
        errorView = findViewById(R.id.error_layout)
        currenciesView = findViewById(R.id.main_recycler_view)
        loadingView = findViewById(R.id.loading_layout)

        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener { _ -> viewModel.requestCurrencies() }

        initRecyclerView()
    }

    private fun setupObservers(){
        viewModel.apply{

            openCurrencyEvent.observe(this@CurrenciesActivity, Observer {currency ->
                if (currency != null) openCurrencyDetails(currency)
            })

            items.observe(this@CurrenciesActivity, Observer {
                if (it != null && !it.isEmpty()) adapter?.setData(it)
            })

            dataLoading.observe(this@CurrenciesActivity, Observer {
                if (it != null){
                    if (it) showLoading()
                    else hideLoading()
                }
            })

            contentVisible.observe(this@CurrenciesActivity, Observer {
                if (it != null){
                    if (it) showContent()
                    else hideContent()
                }
            })

            errorHappened.observe(this@CurrenciesActivity, Observer {
                if (it != null){
                    if (it) showError()
                    else hideError()
                }
            })

        }
    }

    private fun getCurrencyClickedListener(): CurrencyItemUserActionsListener =
        object: CurrencyItemUserActionsListener{
            override fun onCurrencyClicked(currency: Currency) {
                viewModel.openCurrency(currency)
            }
        }


    private fun showLoading() {
        loadingView?.visibility = View.VISIBLE
    }

    private fun hideLoading() {
        loadingView?.visibility = View.GONE
    }

    private fun showError(){
        errorView?.visibility = View.VISIBLE
    }

    private fun hideError(){
        errorView?.visibility = View.GONE
    }

    private fun showContent(){
        currenciesView?.visibility = View.VISIBLE
    }

    private fun hideContent(){
        currenciesView?.visibility = View.GONE
    }

    private fun initRecyclerView() {
        val recyclerView = findViewById<RecyclerView>(R.id.main_recycler_view)
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = CurrenciesAdapter(currencyClickedListener)
        recyclerView.setAdapter(adapter)
    }

    override fun openCurrencyDetails(currency: Currency) {
        val intent = Intent(this, CurrencyDetailsActivity::class.java).apply {
            putExtra(CurrencyDetailsActivity.CURRENCY_JSON, currency.toJson())
        }
        startActivity(intent)
    }

    private fun obtainViewModel(): CurrenciesViewModel = obtainViewModel(CurrenciesViewModel::class.java)
}
