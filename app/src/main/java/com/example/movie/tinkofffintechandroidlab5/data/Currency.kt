package com.example.movie.tinkofffintechandroidlab5.data

import android.arch.persistence.room.Entity
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

@Entity(primaryKeys = ["id"])
data class Currency(
    @field:SerializedName("id")
    val id: String,
    @field:SerializedName("name")
    val name: String?,
    @field:SerializedName("symbol")
    val symbol: String?,
    @field:SerializedName("rank")
    val rank: String?,
    @field:SerializedName("price_usd")
    val priceUsd: String?,
    @field:SerializedName("price_btc")
    val priceBtc: String?,
    @field:SerializedName("24h_volume_usd")
    val _24hVolumeUsd: String?,
    @field:SerializedName("market_cap_usd")
    val marketCapUsd: String?,
    @field:SerializedName("available_supply")
    val availableSupply: String?,
    @field:SerializedName("total_supply")
    val totalSupply: String?,
    @field:SerializedName("max_supply")
    val maxSupply: Any?,
    @field:SerializedName("percent_change_1h")
    val percentChange1h: Double = 0.toDouble(),
    @field:SerializedName("percent_change_24h")
    val percentChange24h: Double = 0.toDouble(),
    @field:SerializedName("percent_change_7d")
    val percentChange7d: Double = 0.toDouble(),
    @field:SerializedName("last_updated")
    val lastUpdated: Long = 0
){
    companion object {
        val gsonObj = Gson()

        fun fromJson(json: String) = gsonObj.fromJson<Currency>(json, Currency::class.java)
    }

    fun toJson() = gsonObj.toJson(this)
}
