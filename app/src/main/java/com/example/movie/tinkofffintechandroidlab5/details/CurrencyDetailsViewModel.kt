package com.example.movie.tinkofffintechandroidlab5.details

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.example.movie.tinkofffintechandroidlab5.data.Currency
import com.example.movie.tinkofffintechandroidlab5.data.source.CurrencyRepository

class CurrencyDetailsViewModel(private val currenyRepository: CurrencyRepository): ViewModel(){

    val item: MutableLiveData<Currency> = MutableLiveData()

    fun start(json: String){
        getCurrencyFromJson(json)
    }

    fun getCurrencyFromJson(json: String){
        val currency = Currency.fromJson(json)
        item.postValue(currency)
    }
}