package com.example.movie.tinkofffintechandroidlab5.currencies

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.movie.tinkofffintechandroidlab5.R
import com.example.movie.tinkofffintechandroidlab5.data.Currency
import com.example.movie.tinkofffintechandroidlab5.utils.setImageFromUrl
import com.example.movie.tinkofffintechandroidlab5.utils.setTextPercentChangeAndAppropriateColor
import com.example.movie.tinkofffintechandroidlab5.utils.setTextPrice
import java.util.ArrayList

class CurrenciesAdapter(private val listener: CurrencyItemUserActionsListener) : RecyclerView.Adapter<CurrenciesAdapter.CoinsViewHolder>() {

    private var items: List<Currency> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoinsViewHolder {
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_coin, parent, false)
        return CoinsViewHolder(layout, listener)
    }

    override fun onBindViewHolder(holder: CoinsViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setData(newItems: List<Currency>) {
        items = newItems
        notifyDataSetChanged()
    }

    class CoinsViewHolder(itemView: View, private val listener: CurrencyItemUserActionsListener) :
        RecyclerView.ViewHolder(itemView) {

        var context: Context
        var coinName: TextView
        var coinPrice: TextView
        var coinChange: TextView
        var coinLogo: ImageView

        init {
            context = itemView.context
            coinName = itemView.findViewById(R.id.coin_name)
            coinPrice = itemView.findViewById(R.id.coin_price)
            coinChange = itemView.findViewById(R.id.coin_change)
            coinLogo = itemView.findViewById(R.id.coin_logo)

        }

        fun bind(currency: Currency) {
            itemView.setOnClickListener { _ -> listener.onCurrencyClicked(currency) }
            coinName.text = currency.name
            coinPrice.setTextPrice(currency.priceUsd)
            coinChange.setTextPercentChangeAndAppropriateColor(currency.percentChange7d)
            val logoUrl = context.getString(R.string.coin_logo_url, currency.symbol!!.toLowerCase())
            coinLogo.setImageFromUrl(itemView, logoUrl)
        }
    }
}
