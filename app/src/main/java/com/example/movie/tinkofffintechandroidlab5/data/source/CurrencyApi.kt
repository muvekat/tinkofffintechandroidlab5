package com.example.movie.tinkofffintechandroidlab5.data.source

import com.example.movie.tinkofffintechandroidlab5.data.Currency
import retrofit2.Call
import retrofit2.http.GET

interface CurrencyApi {

    @GET("ticker/?limit=20")
    fun getCoinsList(): Call<List<Currency>>

}